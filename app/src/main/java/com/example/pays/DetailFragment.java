package com.example.pays;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import static com.example.pays.data.Country.countries;

public class DetailFragment extends Fragment {

    TextView itemPays;
    ImageView itemImage;
    TextView itemCapitale;
    TextView itemLangue;
    TextView itemMonnaie;
    TextView itemPopulation;
    TextView itemSuperficie;
    int country_id;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemPays = view.findViewById(R.id.pays);
        itemImage = view.findViewById(R.id.image);
        itemCapitale = view.findViewById(R.id.capitale);
        itemLangue = view.findViewById(R.id.langue);
        itemMonnaie = view.findViewById(R.id.monnaie);
        itemPopulation = view.findViewById(R.id.population);
        itemSuperficie = view.findViewById(R.id.superficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        country_id = args.getCountryId();
        itemPays.setText(countries[country_id].getName());
        String uri = countries[country_id].getImgUri();
        Context c = itemImage.getContext();
        itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources().getIdentifier (uri, null , c.getPackageName())));
        itemCapitale.setText(countries[country_id].getCapital());
        itemLangue.setText(countries[country_id].getLanguage());
        itemMonnaie.setText(countries[country_id].getCurrency());
        itemPopulation.setText(countries[country_id].getPopulation());
        itemSuperficie.setText(countries[country_id].getArea());

        view.findViewById(R.id.button_retour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}